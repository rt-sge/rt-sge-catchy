package at.ac.tuwien.ifs.sge.game.catchy.core;


import java.awt.*;
import java.util.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class CatchyBoard {

    public static final int EMPTY_ID = -1;
    public static final int EVADER_ID = -2;

    private final int[][] board;

    private final List<Integer> players;
    private final int nrOfPlayers;
    private final int width;
    private final int height;

    private final Point evaderPosition;

    private final ArrayList<Point> playerPositions = new ArrayList<>();
    private final ArrayList<Boolean> isPlayerMoving = new ArrayList<>();

    public CatchyBoard(int nrOfPlayers, int width, int height) {
        this.width = width;
        this.height = height;
        this.nrOfPlayers = nrOfPlayers;
        players = IntStream.rangeClosed(0, nrOfPlayers - 1)
                .boxed().collect(Collectors.toList());
        board = new int[width][height];
        for (var row : board)
            Arrays.fill(row, EMPTY_ID);
        evaderPosition = new Point(width / 2, height / 2);
        board[evaderPosition.x][evaderPosition.y] = EVADER_ID;
        for (int i = 0; i < nrOfPlayers; i++) {
            playerPositions.add(new Point(i, 0));
            isPlayerMoving.add(false);
            board[i][0] = i;
        }
    }

    public CatchyBoard(CatchyBoard board) {
        this.width = board.getWidth();
        this.height = board.getHeight();
        this.nrOfPlayers = board.getNumberOfPlayers();
        this.players = new ArrayList<>(board.getPlayers());
        this.board = Arrays.stream(board.getBoard()).map(int[]::clone).toArray(int[][]::new);
        this.evaderPosition = new Point(board.getEntityPosition(EVADER_ID));
        for (int i = 0; i < nrOfPlayers; i++) {
            playerPositions.add(new Point(board.getPosition(i)));
            this.isPlayerMoving.add(board.isPlayerMoving(i));
        }
    }

    public void setPlayerMoving(int playerId, boolean moving) {
        isPlayerMoving.set(playerId, moving);
    }

    public boolean isPlayerMoving(int playerId) {
        return isPlayerMoving.get(playerId);
    }

    public int getNumberOfPlayers() {
        return nrOfPlayers;
    }

    public List<Integer> getPlayers() {
        return players;
    }

    public Set<CatchyAction> getPossibleActions() {
        var actions = new HashSet<CatchyAction>();
        for (int i = 0; i < nrOfPlayers; i++)
            actions.addAll(getPossibleActions(i));
        return actions;
    }

    public Set<CatchyAction> getPossibleActions(int entityId) {
        var actions = new HashSet<CatchyAction>();
        var position = getEntityPosition(entityId);
        if (position.x > 0 && board[position.x - 1][position.y] <= EMPTY_ID)
            actions.add(CatchyAction.left(entityId));
        if (position.x < width - 1 && board[position.x + 1][position.y] <= EMPTY_ID)
            actions.add(CatchyAction.right(entityId));
        if (position.y > 0 && board[position.x][position.y - 1] <= EMPTY_ID)
            actions.add(CatchyAction.up(entityId));
        if (position.y < height - 1 && board[position.x][position.y + 1] <= EMPTY_ID)
            actions.add(CatchyAction.down(entityId));
        return actions;
    }

    public boolean isValidPlayerAction(CatchyAction action, int playerId) {
        return playerId == action.getEntityId() && action.isStartMovement();
    }

    public boolean isValidAction(CatchyAction action) {
        return isValidPosition(getNextPositionFromAction(action));
    }

    public boolean isEvaderPosition(Point position) {
        return evaderPosition.equals(position);
    }

    public String toTextRepresentation() {
        StringBuilder text = new StringBuilder("\n");
        var divider = new char[2 * width + 2];
        Arrays.fill(divider, '-');
        text.append(new String(divider)).append("\n");
        for (var y = 0; y < height; y++) {
            text.append("|");
            for (var x = 0; x < width; x++) {
                var id = board[x][y];
                var displayId = Integer.toString(id);
                if (id == EMPTY_ID) displayId = "-";
                else if (id == EVADER_ID) displayId = "x";
                text.append(displayId).append(" ");
            }
            text.append("|");
            text.append("\n");
            if (y < height - 1)
                text.append("\n");
        }
        text.append(new String(divider)).append("\n");
        return text.toString();
    }

    public Point getPosition(int playerId) {
        return playerPositions.get(playerId);
    }

    public int[][] getBoard() {
        return board;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public double getEvaderDistance(int playerId) {
        var playerPos = getEntityPosition(playerId);
        var dx = Math.abs(evaderPosition.x - playerPos.x);
        var dy = Math.abs(evaderPosition.y - playerPos.y);
        return dx + dy;
    }

    public Point getEntityPosition(int entityId) {
        if (entityId == EVADER_ID) return evaderPosition;
        else return playerPositions.get(entityId);
    }

    public void move(int entityId, Point from, Point to) {
        board[from.x][from.y] = EMPTY_ID;
        board[to.x][to.y] = entityId;
    }

    public boolean isValidPosition(Point position) {
        if (position.x < 0
                || position.x >= width
                || position.y < 0
                || position.y >= height)
            return false;
        return board[position.x][position.y] <= EMPTY_ID;
    }

    public Point getNextPositionFromAction(CatchyAction action) {
        return getPositionFromAction(action, true);
    }
    public Point getPrevPositionFromAction(CatchyAction action) {
        return getPositionFromAction(action, false);
    }

    private Point getPositionFromAction(CatchyAction action, boolean next) {
        var oldPos = getEntityPosition(action.getEntityId());
        var direction = 1;
        if (!next) direction = -1;
        var newPos = new Point(oldPos.x, oldPos.y);
        switch (action.getDirection()) {
            case CatchyAction.UP:
                newPos.y = oldPos.y - direction;
                break;
            case CatchyAction.RIGHT:
                newPos.x = oldPos.x + direction;
                break;
            case CatchyAction.DOWN:
                newPos.y = oldPos.y + direction;
                break;
            case CatchyAction.LEFT:
                newPos.x = oldPos.x - direction;
                break;
        }
        return newPos;
    }
}
