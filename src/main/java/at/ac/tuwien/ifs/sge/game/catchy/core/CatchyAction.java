package at.ac.tuwien.ifs.sge.game.catchy.core;

import java.io.Serializable;
import java.util.Objects;

public class CatchyAction implements Serializable {
    public static final int UP = 0;
    public static final int RIGHT = 1;
    public static final int DOWN = 2;
    public static final int LEFT = 3;

    public static CatchyAction up(int playerId) { return new CatchyAction(playerId, CatchyAction.UP); }
    public static CatchyAction right(int playerId) { return new CatchyAction(playerId, CatchyAction.RIGHT); }
    public static CatchyAction down(int playerId) { return new CatchyAction(playerId, CatchyAction.DOWN); }
    public static CatchyAction left(int playerId) { return new CatchyAction(playerId, CatchyAction.LEFT); }


    private final int direction;
    private final int entityId;
    private final boolean startMovement;

    public CatchyAction(int entityId, int direction) {
        this.entityId = entityId;
        this.direction = direction;
        this.startMovement = true;
    }

    public CatchyAction(int entityId, int direction, boolean startMovement) {
        this.entityId = entityId;
        this.direction = direction;
        this.startMovement = startMovement;
    }

    public CatchyAction toMoveAction() {
        if (!isStartMovement()) return this;
        return new CatchyAction(entityId, direction, false);
    }

    public int getDirection() {
        return direction;
    }

    public int getEntityId() {
        return entityId;
    }

    public boolean isStartMovement() {
        return startMovement;
    }

    @Override
    public String toString() {
        return "Entity " + entityId + ": " + (startMovement ? "S" : "") + direction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CatchyAction)) return false;
        CatchyAction action = (CatchyAction) o;
        return direction == action.direction && entityId == action.entityId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(direction, entityId);
    }
}


