package at.ac.tuwien.ifs.sge.agent;

import at.ac.tuwien.ifs.sge.core.agent.AbstractRealTimeHumanAgent;
import at.ac.tuwien.ifs.sge.game.catchy.core.Catchy;
import at.ac.tuwien.ifs.sge.game.catchy.core.CatchyAction;

public class CatchyHumanAgent extends AbstractRealTimeHumanAgent<Catchy, CatchyAction> {

    public static void main(String[] args) {
        int playerId = getPlayerIdFromArgs(args);
        String playerName = getPlayerNameFromArgs(args);
        var agent = new CatchyHumanAgent(playerId, playerName);
        agent.start();
    }

    public CatchyHumanAgent(int playerId, String playerName) {
        super(Catchy.class, playerId, playerName);
    }
}
