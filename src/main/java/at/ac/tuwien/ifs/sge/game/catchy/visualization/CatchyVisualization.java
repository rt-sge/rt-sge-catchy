package at.ac.tuwien.ifs.sge.game.catchy.visualization;

import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.game.catchy.core.Catchy;
import at.ac.tuwien.ifs.sge.game.catchy.core.CatchyAction;
import at.ac.tuwien.ifs.sge.game.catchy.core.CatchyBoard;
import at.ac.tuwien.ifs.sge.gui.interfaces.JavaFXVisualization;
import javafx.scene.Node;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import java.util.Objects;

public class CatchyVisualization implements JavaFXVisualization<Catchy, CatchyAction> {

    private static final float TILE_SIZE = 100;

    private final Logger log;

    private Pane root;
    private Canvas background;
    private Canvas entities;
    private Catchy game;

    public CatchyVisualization(Logger log) {
        this.log = log;
    }

    @Override
    public Node initialize(Catchy game, String[] playerNames, double width, double height) {
        this.game = game;
        var board = game.getBoard();
        width = board.getWidth() * TILE_SIZE;
        height = board.getHeight() * TILE_SIZE;

        background = new Canvas(width, height);
        entities = new Canvas(width, height);
        root = new Pane(background, entities);


        drawBackground();
        drawEntities();
        return root;
    }

    @Override
    public void redraw() {
        //var start = System.nanoTime();
        drawEntities();
        //System.out.println("redraw took: " + (System.nanoTime() - start));
    }

    @Override
    public void applyUpdate(CatchyAction action) {
        //var start = System.nanoTime();
        drawEntities();
        //System.out.println("redraw took: " + (System.nanoTime() - start));
    }

    private void drawEntities() {
        GraphicsContext gc = entities.getGraphicsContext2D();
        gc.clearRect(0, 0, entities.getWidth(), entities.getHeight());

        var board = game.getBoard();
        var entityIds = board.getBoard();

        Image evader = new Image(Objects.requireNonNull(getClass().getResource("evader.png")).toString());
        Image player = new Image( Objects.requireNonNull(getClass().getResource("player.png")).toString() );
        for (var x = 0; x < board.getWidth(); x++) {
            for (var y = 0; y < board.getHeight(); y++) {
                var entity = entityIds[x][y];
                if (entity != CatchyBoard.EMPTY_ID) {
                    drawEntity(evader, player, entityIds[x][y], x, y);
                }
            }
        }
    }

    private void drawEntity(Image evader, Image player, int id, float x, float y) {
        if (id == CatchyBoard.EMPTY_ID) return;

        GraphicsContext gc = entities.getGraphicsContext2D();
        if (id == CatchyBoard.EVADER_ID) {
            gc.drawImage(evader, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
        } else {
            gc.drawImage(player, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
            gc.setFill(Color.WHITE);
            Font theFont = Font.font( "Verdana", FontWeight.BOLD, 16 );
            gc.setFont( theFont );
            gc.fillText( Integer.toString(id), x * TILE_SIZE + TILE_SIZE / 2 - 4 , y * TILE_SIZE + TILE_SIZE / 2 + 20);
        }
    }

    private void drawBackground() {
        var board = game.getBoard();
        Image tile = new Image( Objects.requireNonNull(getClass().getResource("grass.png")).toString() );

        GraphicsContext gc = background.getGraphicsContext2D();
        gc.clearRect(0, 0, background.getWidth(), background.getHeight());

        for (var x = 0; x < board.getWidth(); x++) {
            for (var y = 0; y < board.getHeight(); y++) {
                gc.drawImage(tile, x * TILE_SIZE, y * TILE_SIZE, TILE_SIZE, TILE_SIZE);
            }
        }
    }
}
