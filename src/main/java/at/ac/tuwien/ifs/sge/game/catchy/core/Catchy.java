package at.ac.tuwien.ifs.sge.game.catchy.core;

import at.ac.tuwien.ifs.sge.agent.CatchyHumanAgent;
import at.ac.tuwien.ifs.sge.core.engine.communication.ActionResult;
import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.*;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.core.util.Util;
import at.ac.tuwien.ifs.sge.game.catchy.visualization.CatchyVisualization;
import io.reactivex.rxjava3.subjects.PublishSubject;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;

public class Catchy implements RealTimeGame<CatchyAction, CatchyBoard> {

    public static final int MIN_NR_OF_PLAYERS = 2;
    public static final int DEFAULT_BOARD_SIZE = 6;
    public static final int WALK_SPEED_MS = 1000;
    private static final int NR_OF_THREADS = 1;
    private static final StringGameConfiguration DEFAULT_GAME_CONFIGURATION = new StringGameConfiguration(Integer.toString(DEFAULT_BOARD_SIZE));

    private final Logger log;
    private final CatchyBoard board;
    private final StringGameConfiguration gameConfiguration;
    private final GameClock gameClock;
    private final ActionScheduler<Catchy, CatchyAction> actionScheduler;
    private final PublishSubject<CatchyAction> visualizationUpdateSubject = PublishSubject.create();

    private final AtomicBoolean running = new AtomicBoolean(false);

    private final List<TimedActionRecord<CatchyAction>> actionRecords = new ArrayList<>();

    private final Object gameOverLock = new Object();
    private final AtomicBoolean gameOver = new AtomicBoolean(false);
    private final Random random = new Random();

    private ScheduledExecutorService pool;

    private int winner = CatchyBoard.EMPTY_ID;

    public Catchy(GameConfiguration gameConfiguration, Logger log) { this(gameConfiguration, MIN_NR_OF_PLAYERS, log); }

    public Catchy(GameConfiguration gameConfiguration, int nrOfPlayers, Logger log) {
        this.log = log;
        if (gameConfiguration instanceof StringGameConfiguration configuration)
            this.gameConfiguration = configuration;
        else
            this.gameConfiguration = DEFAULT_GAME_CONFIGURATION;
        var sizeString = this.gameConfiguration.getConfiguration();
        var size = DEFAULT_BOARD_SIZE;
        if (sizeString != null) {
            try {
                size = Integer.parseInt(sizeString);
            } catch (NumberFormatException ignored) {}
        }
        gameClock = new GameClock();
        board = new CatchyBoard(nrOfPlayers, size, size);
        actionScheduler = new ActionScheduler<>(this, log);
        pool = Executors.newScheduledThreadPool(NR_OF_THREADS);
    }

    public Catchy(Catchy catchy) {
        this.log = catchy.getLog();
        this.gameConfiguration = (StringGameConfiguration) catchy.getGameConfiguration();
        this.gameClock = new GameClock(catchy.getGameClock());
        this.board = new CatchyBoard(catchy.getBoard());
        this.actionScheduler = new ActionScheduler<>(this, catchy.getActionScheduler());
    }

    @Override
    public boolean isGameOver() {
        return gameOver.get();
    }

    @Override
    public void waitForGameOver(long timeoutMs) throws InterruptedException {
        var timeoutAt = System.currentTimeMillis() + timeoutMs;
        synchronized (gameOverLock) {
            while (!gameOver.get() && System.currentTimeMillis() < timeoutAt) {
                gameOverLock.wait(timeoutMs);
            }
        }
    }

    @Override
    public int getMinimumNumberOfPlayers() {
        return MIN_NR_OF_PLAYERS;
    }

    @Override
    public int getMaximumNumberOfPlayers() {
        return board.getWidth();
    }

    @Override
    public int getNumberOfPlayers() {
        return board.getNumberOfPlayers();
    }

    @Override
    public double getPlayerUtilityWeight(int player) {
        return 1D;
    }

    @Override
    public double getUtilityValue(int player) {
        return player == winner ? 1D : 0D;
    }

    @Override
    public double getHeuristicValue(int player) {
        return board.getWidth() + board.getHeight() - 2 - board.getEvaderDistance(player);
    }

    @Override
    public Set<CatchyAction> getPossibleActions() {
        return board.getPossibleActions();
    }

    @Override
    public Set<CatchyAction> getPossibleActions(int playerId) {
        return board.getPossibleActions(playerId);
    }

    @Override
    public CatchyBoard getBoard() {
        return board;
    }

    @Override
    public List<TimedActionRecord<CatchyAction>> getActionRecords() {
        return actionRecords;
    }

    @Override
    public boolean isCanonical() {
        return true;
    }

    @Override
    public boolean isRealtime() {
        return true;
    }

    @Override
    public String getHumanAgentClassName() {
        return CatchyHumanAgent.class.getName();
    }

    @Override
    public String getVisualizationClassName() {
        return CatchyVisualization.class.getName();
    }

    @Override
    public VisualizationType getVisualizationType() {
        return VisualizationType.javafx;
    }

    @Override
    public GameClock getGameClock() {
        return gameClock;
    }

    @Override
    public Map<Integer, List<CatchyAction>> initialize() {
        gameClock.start();
        var updates = new ArrayList<GameUpdate<CatchyAction>>();
        for (var i = 0; i < board.getNumberOfPlayers(); i++) {
            var actionEvent = GameActionEvent.internal(new CatchyAction(i, CatchyAction.DOWN));
            updates.addAll(applyActionEvent(actionEvent));
        }
        var initUpdates = new HashMap<Integer, List<CatchyAction>>();
        for (var update : updates) {
            var playerId = update.getPlayer();
            if (initUpdates.containsKey(playerId)) {
                initUpdates.get(playerId).addAll(update.getActions());
            } else {
                initUpdates.put(playerId, new ArrayList<>(update.getActions()));
            }

        }
        return initUpdates;
    }

    @Override
    public void start() {
        running.set(true);
        actionScheduler.setExecutorService(pool);
        actionScheduler.start();
        scheduleNextEvaderMove(gameClock.getGameTimeMs());
    }

    @Override
    public void stop() {
        actionScheduler.stop();
        pool.shutdown();
        running.set(false);
    }

    @Override
    public RealTimeGame<CatchyAction, CatchyBoard> copy() {
        return new Catchy(this);
    }

    @Override
    public void advance(long timeMs) throws ActionException {
        //log.debug("advancing game for " + timeMs + " | current game time: " + gameClock.getGameTimeMs());
        var newTime = gameClock.getGameTimeMs() + timeMs;
        actionScheduler.clearUntil(gameClock.getGameTimeMs());
        actionScheduler.applyUntil(newTime);
        gameClock.setGameTimeMs(newTime);
        //log.debug("setting gametime to: " + newTime);
    }

    @Override
    public boolean isRunning() {
        return running.get();
    }

    @Override
    public GameUpdate<CatchyAction> readGameUpdate() throws InterruptedException {
        return actionScheduler.readGameUpdate();
    }

    @Override
    public void scheduleActionEvent(GameActionEvent<CatchyAction> action) {
        actionScheduler.scheduleActionEvent(action);
    }

    @Override
    public List<GameUpdate<CatchyAction>> applyActionEvent(GameActionEvent<CatchyAction> actionEvent) {
        var action = actionEvent.getAction();
        var executionTime = actionEvent.getExecutionTimeMs();
        var playerId = actionEvent.getPlayerId();

        if (playerId != GameActionEvent.INTERNAL_EVENT_ID &&
                !board.isValidPlayerAction(action, playerId))
            return List.of();

        var result = applyAction(action, executionTime);

        var gameUpdateComposite = new GameUpdateComposite<CatchyAction>(executionTime);
        if (result.wasSuccessful())
            gameUpdateComposite.addActionForPlayers(action, board.getPlayers());

        return gameUpdateComposite.toList();
    }

    @Override
    public ActionResult applyAction(CatchyAction action, long executionTimeMs) {
        if (isGameOver()) return new ActionResult(false);
        var newPos = board.getNextPositionFromAction(action);
        var entityId = action.getEntityId();
        if (board.isValidPosition(newPos)) {
            actionRecords.add(new TimedActionRecord<>(executionTimeMs, action));
            if (action.isStartMovement()) {
                if (board.isPlayerMoving(entityId)) {
                    var actionEventQueue = actionScheduler.getActionEventQueue();
                    actionEventQueue.removeIf(actionEvent -> {
                        var scheduledAction = actionEvent.getAction();
                        return !scheduledAction.isStartMovement() && scheduledAction.getEntityId() == entityId;
                    });
                } else
                    board.setPlayerMoving(entityId, true);
                var executionTime = executionTimeMs + WALK_SPEED_MS;
                var actionEvent = GameActionEvent.internal(action.toMoveAction(), executionTime);
                actionScheduler.scheduleActionEvent(actionEvent);
            } else {
                var entityPosition = board.getEntityPosition(entityId);
                board.move(entityId, entityPosition, newPos);
                entityPosition.setLocation(newPos);

                if (entityId != CatchyBoard.EVADER_ID) {
                    board.setPlayerMoving(entityId, false);
                    synchronized (gameOverLock) {
                        if (board.isEvaderPosition(entityPosition)) {
                            winner = entityId;
                            gameOver.set(true);
                            gameOverLock.notifyAll();
                        }
                    }
                } else scheduleNextEvaderMove(executionTimeMs);
            }
            gameClock.setGameTimeMs(executionTimeMs);
            return new ActionResult(true);
        } else {
            if (entityId == CatchyBoard.EVADER_ID)
                scheduleNextEvaderMove(executionTimeMs);
            return new ActionResult(false);
        }

    }

    @Override
    public String toTextRepresentation() {
        return board.toTextRepresentation();
    }

    @Override
    public GameConfiguration getGameConfiguration() {
        return gameConfiguration;
    }

    public Logger getLog() {
        return log;
    }

    public ActionScheduler<Catchy, CatchyAction> getActionScheduler() {
        return actionScheduler;
    }

    private void scheduleNextEvaderMove(long currentTime) {
        if (!isRunning()) return; // don't schedule new evader movement when the game is not running (i.e. agent side)
        var actions = board.getPossibleActions(CatchyBoard.EVADER_ID);
        CatchyAction evaderAction = CatchyAction.up(CatchyBoard.EVADER_ID).toMoveAction();
        if (actions.size() > 0) {
            evaderAction = Util.selectRandom(actions, random);
        }
        var executionTime = currentTime + WALK_SPEED_MS * 2;
        actionScheduler.scheduleActionEvent(GameActionEvent.internal(evaderAction.toMoveAction(), executionTime));
    }
}
