module sge.game.catchy {
    requires sge.core;
    requires sge.gui;
    requires io.reactivex.rxjava3;
    requires java.desktop;
    requires javafx.graphics;

    exports at.ac.tuwien.ifs.sge.game.catchy.core;
}
