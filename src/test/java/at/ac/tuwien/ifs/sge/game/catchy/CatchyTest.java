package at.ac.tuwien.ifs.sge.game.catchy;

import at.ac.tuwien.ifs.sge.core.engine.communication.events.GameActionEvent;
import at.ac.tuwien.ifs.sge.core.engine.logging.Logger;
import at.ac.tuwien.ifs.sge.core.game.StringGameConfiguration;
import at.ac.tuwien.ifs.sge.core.game.exception.ActionException;
import at.ac.tuwien.ifs.sge.game.catchy.core.Catchy;
import at.ac.tuwien.ifs.sge.game.catchy.core.CatchyAction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.awt.*;

import static org.junit.jupiter.api.Assertions.*;

public class CatchyTest {

    private Catchy game;

    @BeforeEach
    public void init() {
        var log = new Logger(-2, "[sge ", "",
                "trace]: ", System.out, "",
                "debug]: ", System.out, "",
                "info]: ", System.out, "",
                "warn]: ", System.err, "",
                "error]: ", System.err, "");
        game = new Catchy(new StringGameConfiguration(""), 2, log);

    }

    @Test
    public void possbile_action_amount_correct() {
        var actions = game.getPossibleActions(0);
        assertEquals(1, actions.size());
    }

    @Test
    public void apply_possible_action() throws ActionException {
        var actions = game.getPossibleActions(0);
        assertEquals(1, actions.size());
        var action = actions.stream().findFirst();
        assertTrue(action.isPresent());
        game.applyActionEvent(new GameActionEvent<>(0, action.get(), System.currentTimeMillis() + 5));
        game.advance(Catchy.WALK_SPEED_MS + 10);
        var pos = game.getBoard().getPosition(0);
        assertEquals(pos, new Point(0,1));
    }

    @Test
    public void change_movement_direction() throws ActionException {
        game.applyActionEvent(new GameActionEvent<>(1, CatchyAction.right(1), System.currentTimeMillis() + 5));
        game.advance(100);
        game.applyActionEvent(new GameActionEvent<>(1, CatchyAction.down(1), System.currentTimeMillis() + 5));
        game.advance(Catchy.WALK_SPEED_MS + 10);
        var pos = game.getBoard().getPosition(1);
        assertEquals(pos, new Point(1,1));
    }

}
